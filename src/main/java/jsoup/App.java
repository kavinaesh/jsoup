package jsoup;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import org.apache.commons.io.FileUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class App {
    public static void main(String[] args) throws IOException {
        CSVReader csvReader = new CSVReader(new FileReader("/home/user/Downloads/kvar3.csv"));
        CSVWriter csvWriter = new CSVWriter(new FileWriter("/home/user/Downloads/kvar5.csv"));
        String[] nextLine;
        while ((nextLine = csvReader.readNext()) != null) {
            String companyName = nextLine[0];

            System.out.println("Going to Fetch For " + companyName);
            justDialFetch(companyName);
            grotalFetch(companyName);

        }


    }

    private static void grotalFetch(String companyName) {
        String url = "http://www.grotal.com/Bangalore/" + companyName.replaceAll(" ", "-") + "-C67A0P1A0/";
        System.out.println("Fetching URL " + url);
        try {
            Connection connect = Jsoup.connect(url);
            Document document = connect.get();
            String body = document.html();

            FileUtils.writeStringToFile(new File("/tmp/grotal-" + companyName.hashCode() + ".html"), body);
            Elements results = document.select("div.result.fl.w100");
            int count = 0;
            for (Element sa : results) {

                if (count++ > 1) {
                    break;
                }


                String name = sa.select("div.fl.w30.tl").first().text();
                String address = sa.select("div.fl.w20.tl").first().text() +
                        sa.select("div.fl.w35.tl").first().text();
                String phno = "";

                Element first = sa.select("div.fl.w15.tl").first();
                if (first != null) {
                    List<TextNode> textNodes = first.textNodes();

                    for (TextNode tn : textNodes) {
                        phno += tn.text();
                    }
                }


                Element a = sa.select("div.fl.w70.tl").first();
                String indutry = a == null ? "" : a.text();
                System.out.println(String.format("Grotal::: Name:[%s], Address[%s], PhNo[%s], Industry[%s]",
                        name, address, phno, indutry));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void justDialFetch(String companyName) {
        String url = "http://www.justdial.com/Bangalore/" + companyName;
        try {
            Connection connect = Jsoup.connect(url);
            Document document = connect.get();
            String body = document.html();

            FileUtils.writeStringToFile(new File("/tmp/justdial-" + companyName.hashCode() + ".html"), body);
            Elements results = document.select("div.sa");
            int count = 0;
            for (Element sa : results) {

                if (count++ > 1) {
                    break;
                }
                Element re = sa.select("div.result").first();
                String name = re.select("span.title").first().text();
                String address = re.select("span.address").first().text();
                String phno = sa.select("span.ph").first().select("a").first().text();
                Element a = sa.select("span.lstAlso").first().select("a").first();
                String indutry = a == null ? "" : a.text();
                System.out.println(String.format("JustDial::: Name:[%s], Address[%s], PhNo[%s], Industry[%s]",
                        name, address, phno, indutry));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
//http://www.grotal.com/Bangalore/almas-enterprisis-C67A0P1A0/
